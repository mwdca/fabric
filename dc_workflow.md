# DC workflow

The demo setup in this repo contains config templates for DC gateway, spine and leaf network devices plus site specific variables in the folders [zug1](zug1/) and [luzern1](luzern1/).
Lets make some simple loopback IP address changes in site zug1. To make changes to the device configuration, you need to clone the repo, create and checkout a working branch. 

```
$ git clone git@gitlab.com:mwdca/fabric.git
$ cd fabric
$ git branch loopback-ip-change
$ git checkout loopback-ip-change
```

### Make your changes

Start making your changes. Check all your changes to the folder zug1 using `git diff zug1`:

```
$ git diff zug1/
diff --git a/zug1/dcgw.yml b/zug1/dcgw.yml
index a2eab66..0f992f2 100644
--- a/zug1/dcgw.yml
+++ b/zug1/dcgw.yml
@@ -1,7 +1,7 @@
 ---
 - host_name: dcgw1
   management_ip: 192.168.2.31
-  route_distinguisher_id: 10.6.6.31
+  route_distinguisher_id: 10.6.0.31
   fabric_links:
     provision:
       interface: xe-0/0/0
@@ -27,7 +27,7 @@
         peer_as: 64022
 - host_name: dcgw2
   management_ip: 192.168.2.32
-  route_distinguisher_id: 10.6.6.32
+  route_distinguisher_id: 10.6.0.32
   fabric_links:
     provision:
       interface: xe-0/0/0
diff --git a/zug1/spine.yml b/zug1/spine.yml
index 06d110f..401bee9 100644
--- a/zug1/spine.yml
+++ b/zug1/spine.yml
@@ -24,11 +24,11 @@
     local_as: 64021
     neighbors:
       neighbor_1:
-        ip: 10.6.6.31
+        ip: 10.6.0.31
         peer_as: 64031
         local_interface: xe-0/0/1
       neighbor_2:
-        ip: 10.6.6.32
+        ip: 10.6.0.32
         peer_as: 64032
         local_interface: xe-0/0/2
       neighbor_3:
@@ -64,11 +64,11 @@
     local_as: 64022
     neighbors:
       neighbor_1:
-        ip: 10.6.6.31
+        ip: 10.6.0.31
         peer_as: 64031
         local_interface: xe-0/0/1
       neighbor_2:
-        ip: 10.6.6.32
+        ip: 10.6.0.32
         peer_as: 64032
         local_interface: xe-0/0/2
       neighbor_3:
```

### Sync with master before commit

Now we are nearly ready to commit the changes locally first, but it is good practice to incorporate any potential changes added to master. There are many ways to do so (google it!), but for me this worked pretty well so far:

```
$ git checkout master                                                                    
$ git pull                                                                               
$ git checkout loopback-ip-change
$ git merge master
```

This changes the local focus to master branch, pulls remote changes down from gitlab, then changes the focus back to the working branch (loopback-ip-change in this workflow) and merges changes from the master branch. Don't worry about the work we've before changing branches. It is still there at the end of above commands.

### Check for syntax errors locally

While the gitlab CI pipeline will have you covered by preventing syntax errors to make it onto the devices, the failed pipeline will point to the errors with your signature on it.
The same [render.py](render.py) script is also executed in the first stage of the CI pipeline and it can be run locally. The rendered device config files can be found in the [configs/](configs/) folder.

```
$ python3 render.py

rendering images ...

rendering zug1 ...
spine.yml
building configuration files ...
generate config file configs/zug1/spine1.conf.txt ...
generate config file configs/zug1/spine2.conf.txt ...
leaf.yml
building configuration files ...
generate config file configs/zug1/leaf1.conf.txt ...
generate config file configs/zug1/leaf2.conf.txt ...
dcgw.yml
building configuration files ...
generate config file configs/zug1/dcgw1.conf.txt ...
generate config file configs/zug1/dcgw2.conf.txt ...

rendering luzern1 ...
spine.yml
building configuration files ...
generate config file configs/luzern1/spine1L.conf.txt ...
generate config file configs/luzern1/spine2L.conf.txt ...
leaf.yml
building configuration files ...
generate config file configs/luzern1/leaf1L.conf.txt ...
generate config file configs/luzern1/leaf2L.conf.txt ...
dcgw.yml
building configuration files ...
generate config file configs/luzern1/dcgw1L.conf.txt ...
generate config file configs/luzern1/dcgw2L.conf.txt ...

done
```

### Commit the changes

Check which files you changed via `git status`, then add the ones you want to commit. Wildcards can be used. Running `git status` after `git add` will list the files staged for commit.
Finally, commit the changes by providing a meaningful commit message. 

```
$ git status
$ git add zug1/
$ git status
$ git commit -m 'change loopback ip back to 10.6.0.*'
```

### Push committed changes to gitlab

```
$ git push
```

If this is the first time you try this command on your local branch, git will ask you to set the upstream origin. Just copy paste the command and your changes will make it to gitlab.

```
$ git push --set-upstream origin loopback-ip-change
```

Launch your browser and check your repos gitlab pipeline. It got triggered automatically, rendering your changes.

![render_branch_push](images/render_branch_push.jpg)

### Create merge request

To get the branch deployed and tested, create a pull request via gitlab webUI. Select your repo, then click on 'Repository' -> 'Branches' on the left menu column. Look for your work branch under active branches, then click on 'Merge request'.

![merge_request](images/merge_request.jpg)

Assign the merge request to yourself and add any required approvers/reviewers you want/need before the changes are deployed, then hit the green 'Submit merge request' button on the bottom of the page.

![merge_request_submit](images/merge_request_submit.jpg)

Sit back, relax and watch how rendering and staging automatically kick in. Nothing will get deployed yet. By design that step requires a manual trigger. 

![merge_request_pipeline](images/merge_request_pipeline.jpg)

Once rendering and staging are complete, the pipeline stage icon will change and a bold text 'Pipeline #123456 waiting for manual action for ...' will be shown. If you got this far, not only was the configuration change rendered properly, it was also sent to every device in the affected site for a successful 'commit check'.

![merge_request_manual](images/merge_request_manual.jpg)

You can either click on the gear symbol on this page or head over to the pipeline UI. But check the staging task first for success. The pipeline is currently built to allow an operator to trigger the deployment while the staging is ongoing or even failed. This is to work urgent cases that require an immediate rollout without waiting for staging to complete. While this staging task completes in less than a minute, production quality staging tasks may run for hours using dedicated staging labs.

![pipeline_check_staging](images/pipeline_check_staging.jpg)

All good here, so click the gear wheel next to zug1-deploy. Use the gitlab Web UI to go to CI/CD -> Pipelines and click on the running pipeline for this merge request.

![pipeline_deploy](images/pipeline_deploy.jpg)

The stage deploy is executing now, followed by the verify stage and final commit stage, if all goes well. As we deal here with live systems, there is always a chance for transient errors. Such can be overcome by re-running individual stages.

![pipeline_success](images/pipeline_success.jpg)

The final task now is to actually merge the changes into master, allowing master to reflect the actual state of the devices. 

![pipeline_merge](images/pipeline_merge.jpg)

All done !!

![pipeline_merge_complete](images/pipeline_merge_complete.jpg)

Go to the repository master and check the commit note next to the zug1 folder. 

![commit_message_after_merge](images/commit_message_after_merge.jpg)







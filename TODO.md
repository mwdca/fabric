While working on the DC workflow document, I encountered two minor issues worth exploring further:

- [Issue #1](https://gitlab.com/mwdca/fabric/issues/1) enabling netconf rfc-compliant mode on vMX led to PyEz XMLSyntaxError messages while running ccheck.py. Removing rfc-compliant fixed it. If the config is checked manually via CLI 'commit check', no problem was found. Removed rfc-compliant.
- [Issue #2](https://gitlab.com/mwdca/fabric/issues/2) Applying the loopback IP change removed the unnumbered IP from the xe-0/0/x interfaces on vMX. A manual 'commit full and-quit' solved this. Not sure if this is vMX related or a general issue in 18.4R1 when changing loopback IP used as unnumbered reference for xe interfaces or not.

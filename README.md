# DC Fabric CICD pipeline

This sample repo explores the gitlab CI/CD pipeline to build (render), deploy and test Junos configurations for DC gateway, Spine and Leaf switches to one or more Datacenters. It builds upon the great work shared by ksator at <https://github.com/ksator/python-training-for-network-engineers>.

## Goals

- Render all configs from Jinja2 templates and site specific variables
- Validate configs on actual target devices via commit check
- Require manual trigger to deploy configs per DC site, even if the staging failed (the idea here is to have a more comprehensive staging, potentially running for longer than the operator is comfortable rolling out an emergency change)
- The deployed configuration shall be validated in a testing stage with a built-in automatic rollback using commit confirmed
- Finally, if all tests across all DC sites succeeded, the confirmed running config shall be committed for eternity

## Workflow

Please check [DC workflow](dc_workflow.md) for an complete workflow changing configuration, render, deploy and test them using a git branch.

The [DC CICD automation demo video](https://youtu.be/2OaBNjWWYj0) shows the workflow of a configuration change attempt that failed during the test/validation stage. All devices are automatically reverting back to their previous state. This demo shows the "transactional nature" of configuration changes: all devices either keep the change or roll back.


## gitlab runners

Each DC site requires a gitlab runner to be registered with a tag set to its site name.
It will pick up jobs for the local site. For this demo repo, two virtual labs are deployed on a compute node, each consisting of 2 DC GW, 2 spine and 2 leaf switches based on vMX and vQFX.
Virtual lab toplogies <https://gitlab.com/mwdca/vlabzug1> and <https://gitlab.com/mwdca/vlabluzern1>) are launched via docker-compose including automatically registered gitlab runners per lab:

![vlab diagram](images/vlab-diagram.jpg)

The container based runners are using the Juniper/pyez container and access the virtual devices via netconf using a ssh private rsa key stored in the CICD environment settings.
The actual "work" is done in this demo in Python3 using PyEz without much error checking to keep the code easy consumable and within a few lines.

## Pipeline

This "proof-of-concept" pipeline uses 4 stages: Render, Deploy, Verify and Commit, with parallel execution of jobs within each stage per datacenter.

![successful pipeline](images/successful_pipeline.jpg)

Each Datacenter runs a local gitlab runner that is registered to gitlab using its site name as tag. This allows each runner to pick up tasks relevant only to its own fabric.

The [.gitlab-ci.yml](.gitlab-ci.yml) file makes use of various gitlab pipeline features like 'extends', 'allow failure', 'artefacts' and 'only when changed patterns' etc to keep the file readable and avoid redundant entries per site and stage.

The big picture idea is for operators to work on branches and request changes via pull requests into master or a production branch. Additional branches can be introduced between operator and production site, e.g. to stage changes first in a virtual lab or physical functional and/or performance labs.

### Render stage

The render stage creates the device configuration files using templates found in the templates folder with site specific variables from the site folders, e.g. zug1/ and luzern1/, found in a file per device type. The gitlab-ci.yml pipeline definition tries to execute only stages that have files changes.

The resulting configs are stored in subdirectories per site under configs/ and made available as artefacts in gitlab. Subsequent stages have direct access to these files directly from the generated folders. Alternatively, the artefacts could be pushed to a remote artifactory
or accessed using the well-known job artefact URLs.

### Deploy stage

The deploy stage has a dual use: once by the staging jobs that validate the configuration files by loading them onto each device and triggering a non-intrusive commit check. This is executed for each DC site triggered by a successful render job.

To actually deploy the configuration onto devices, the deploy jobs require an operator to manually trigger their execution, either for all sites (play button next to the Deploy title) or per DC site. Subsequent stages wait for successful deployment in all DC sites.

To satisfy the automated rollback functionality, the config changes are committed using the commit confirm feature, using a configurable timeout in minutes (set in the gitlab-ci.yml file or via environment variable). It shall be long enough to allow the downstream verification stage to complete across all sites plus the final commit stage.

### Verify stage

The verify stage executes various tests against the activated config, e.g. testing the loopback reachability across the fabric, checking all BGP sessions are established etc.

### Commit stage

The final commit stage commits the temporary configs using a Junos commit. This only happens, once all sites passed the verification stage. If any stage at any site fails, the pipeline is considered failed and the commit confirm on all devices will initiate an automatic rollback to the previous configuration.

### TODO

Add actual test cases to the verify stage. Idea's evolve around using JSNAPy and/or multiping and verifying the BGP peer states etc.

### Looking for more Junos automation solutions

<https://github.com/ksator?tab=repositories>  
<https://gitlab.com/users/ksator/projects>  
<https://gist.github.com/ksator/>
<https://gitlab.com/mwiget/bgp-unnumbered/>

### Contributions, bugs, questions or enhancement requests

please submit github issues or pull requests.  



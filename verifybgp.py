""" veryifybgp.py - verify all BGP sessions are established """

import os
import sys
import argparse
import warnings
import jxmlease
import yaml

from lxml import etree
from jnpr.junos import Device
# ignore CryptographyDeprecationWarning
warnings.filterwarnings(action='ignore', module='.*paramiko.*')

parser = argparse.ArgumentParser()
parser.add_argument('--site', help='DC site name')
parser.add_argument('--user', help='device login username')
args = parser.parse_args()

if args.site is None:
    print('please specify site name using --site <name> argument')
    exit(1)

if args.user is None:
    print('please specify device login name using --user <name> argument')
    exit(1)

print('\nverify BGP sessions in site ' + args.site + ' ...\n')

confpath = 'configs/' + args.site
if not os.path.isdir(confpath):
    print('no configs folder ' + confpath + ' found')
    exit(1)

SUCCESS = True

for devicefile in os.listdir(args.site):
    if not devicefile.endswith('.yml') or devicefile.startswith('.'):
        continue

    devicetype = devicefile[:-4]

    fv = open(args.site+'/'+devicefile)
    data = fv.read()
    my_vars = yaml.load(data, Loader=yaml.SafeLoader)
    fv.close()

    for device in my_vars:
        conffilename = confpath+'/'+device["host_name"]+'.conf.txt'
        dev = Device(host=device["management_ip"], user=args.user)
        dev.open()
        print(device["host_name"] +
              ' (' + device['management_ip'] + ') running ' + dev.facts["version"] + ' ...')

        rpc = dev.rpc.get_bgp_summary_information()
        rpc_xml = etree.tostring(rpc, pretty_print=True, encoding='unicode')
        xmlparser = jxmlease.Parser()
        result = jxmlease.parse(rpc_xml)

        peerstate = dict()
        peeras = dict()
        for neighbor in result['bgp-information']['bgp-peer']:
            peer = neighbor['peer-address']
            peerstate[peer] = neighbor['peer-state']
            peeras[peer] = neighbor['peer-as']
            print('  BGP peer ' + peer + ' AS ' +
                  peeras[peer] + ' ' + peerstate[peer])

        for index in device["bgp"]["neighbors"]:
            cfgneighbor = device["bgp"]["neighbors"][index]
            if peerstate[cfgneighbor["ip"]] != 'Established' or \
                    str(peeras[cfgneighbor["ip"]]) != str(cfgneighbor["peer_as"]):
                print('Error: ' + cfgneighbor["ip"] + ' peer-as ' + str(
                    cfgneighbor["peer_as"]) + ': ' + peerstate[cfgneighbor["ip"]] + ': ' +
                      peerstate[cfgneighbor["ip"]] + ' ' + peeras[cfgneighbor["ip"]])

                SUCCESS = False

        dev.close()

if not SUCCESS:
    print("at least one BGP session isnt up yet")
    sys.exit(1)

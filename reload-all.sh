#!/bin/bash
# render, load and commit configs to all sites

set -e  # exit immediately on error

python3 render.py
for site in zug1 luzern1; do
  echo $site ...
  python3 cconfirm.py --site $site --user $USER
  python3 verifybgp.py --site $site --user $USER
  python3 ccommit.py --site $site --user $USER
done

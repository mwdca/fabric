#!/bin/bash
# quick and dirty upload and commit for testing
# Requires network access via ssh port forwarding, e.g.
# 
# Host leaf1
 # HostName 192.168.2.51
 # User mwiget
 # IdentityFile ~/.ssh/id_rsa_vlabzug1
 # ProxyCommand ssh -q -W %h:%p baremetalserver

for config in configs/zug1/*; do
  device=$(basename $config | cut -d. -f1)
  file=$(basename $config)
  echo $device: $config ...
  scp -o StrictHostKeyChecking=no $config $device:
  ssh $device "config; load replace $file; commit"
done

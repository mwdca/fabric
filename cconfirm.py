""" cconfirm.py - commit confirm """

import os
import argparse
import warnings
import yaml
from jnpr.junos import Device
from jnpr.junos.utils.config import Config

# ignore CryptographyDeprecationWarning
warnings.filterwarnings(action='ignore', module='.*paramiko.*')

parser = argparse.ArgumentParser()
parser.add_argument('--site', help='DC site name')
parser.add_argument('--user', help='device login username')
parser.add_argument('--confirm', type=int, default=10, help='confirm timeout in minutes')
args = parser.parse_args()

if args.site is None:
    print('please specify site name using --site <name> argument')
    exit(1)

if args.user is None:
    print('please specify device login name using --user <name> argument')
    exit(1)

print('\ncommit confirmed (' + str(args.confirm) + ' minutes) for site ' + args.site + ' ...\n')

confpath = 'configs/' + args.site
if not os.path.isdir(confpath):
    print('no configs folder ' + confpath + ' found')
    exit(1)

for devicefile in os.listdir(args.site):
    if not devicefile.endswith('.yml') or devicefile.startswith('.'):
        continue

    devicetype = devicefile[:-4]

    fv = open(args.site+'/'+devicefile)
    data = fv.read()
    my_vars = yaml.load(data, Loader=yaml.SafeLoader)
    fv.close()

    for device in my_vars:
        conffilename = confpath+'/'+device["host_name"]+'.conf.txt'
        print('checking config file '+conffilename +
              ' on device ' + device["host_name"] + ' mgmt ip ' + device['management_ip'] + ' ...')
        dev = Device(host=device["management_ip"], user=args.user)
        dev.open()
        cfg = Config(dev)
        cfg.rollback()  # Execute Rollback to prevent commit change from old config session
        cfg.load(path=conffilename, format='text')
        cfg.pdiff()
        # using udpate mode to keep the apply-group untouched on the device (required mainly
        # for qfx to keep the dynamic mac address on the revenue ports
        if cfg.commit(confirm=args.confirm):
            print('commit confirmed in ' + str(args.confirm) + '  minutes on ' +
                  dev.facts["hostname"])
        else:
            print('commit confirmed failed on ' + dev.facts["hostname"])
        dev.close()

print('done')
